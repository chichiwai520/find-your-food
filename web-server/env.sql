/* Put These into your .env: */
DB_NAME=find_restaurant
DB_USERNAME=foodie
DB_PASSWORD=123456


/* Insert these to create a new database and user in your PosgreSQL: */
CREATE USER foodie WITH PASSWORD '123456' SUPERUSER;
CREATE DATABASE find_restaurant;

/* Login to your database "foodie" with password "123456": */
psql -U foodie -W -h localhost find_restaurant;